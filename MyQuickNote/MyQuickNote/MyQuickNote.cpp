// MyQuickNote.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyQuickNote.h"
#include <windowsx.h>
#include <CommCtrl.h>
#define MAX_LOADSTRING 100
#pragma warning(disable:4996)  

HFONT hFont;


// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK AddNote(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK NewTags(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK Preview(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK Chart(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MYQUICKNOTE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYQUICKNOTE));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYQUICKNOTE));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYQUICKNOTE);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_MYQUICKNOTE));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      350, 170, 605, 330, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }
   
   ShowWindow(hWnd, SW_HIDE);
   UpdateWindow(hWnd);

   // Ẩn ứng dụng khi khởi động
   ZeroMemory(&notifyApp, sizeof(notifyApp));
   notifyApp.cbSize = NOTIFYICONDATAA_V3_SIZE;
   notifyApp.hWnd = hWnd;
   notifyApp.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
   notifyApp.uCallbackMessage = WM_TRAY_ICON;
   notifyApp.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_NOTIFYAPP));
   notifyApp.uVersion = NOTIFYICON_VERSION_4;

   StringCchCopy(notifyApp.szTip, ARRAYSIZE(notifyApp.szTip), L"Quick Note"); // Hiện tên app khi trỏ vào icon đc ẩn
   Shell_NotifyIcon(NIM_ADD, &notifyApp);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
bool invisibleNode;
HWND hwndDialog;
HWND hNewTag;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{
		allNote.clear();
		allTags.clear();
		invisibleNode = false;
		loadTagsFromFile();
		loadNotesFromFile();
		lineOfListView = 0;
		/*
		for (int i = 0; i < allNote.size(); i++)
		{
			MessageBox(hWnd, allNote[i].getContentNote(), 0, 0);
			for (int j = 0; j < allNote[i].getSizeTagOfNote(); j++)
			{
				MessageBox(hWnd, allNote[i].getPosOfTag(j), 0, 0);
			}
		}*/
		
		OnCreate(hWnd);
		//ZeroMemory(&allNote, sizeof(allNote));
		//ZeroMemory(&allTags, sizeof(allTags));
		//allTags.clear();
		doInstallHook(hWnd);
	}
	break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case ID_NOTIFYMENU_BROWSER:
			{
				ShowWindow(hWnd, SW_NORMAL);

			}
				break;
			case ID_NOTIFYMENU_CHART:
			{
				saveTagPaint.clear();
				// Lấy tất cả các tag thêm vào vector saveTagPaint để kiểm tra
				for (int i = 0; i < allTags.size(); i++)
				{
					CTag tag;
					tag.setContentTag(allTags[i]); // Thêm nội dung tag
					saveTagPaint.push_back(tag); // Thêm tag vào để dễ quản lý đếm số note thuộc về tag đó
				}

				// Thực hiện đếm số note thuộc về một tag
				getDataToChart();

				hwndDialog = NULL;  // Window handle of dialog box
				if (!IsWindow(hwndDialog))
				{
					hwndDialog = CreateDialog(hInst,
						MAKEINTRESOURCE(IDD_CHART),
						hWnd,
						(DLGPROC)Chart);

					ShowWindow(hwndDialog, SW_SHOW);
				}
			}
				break;
			case ID_NEWNOTE:
			{
				//DialogBox(hInst, MAKEINTRESOURCE(IDD_ADDNOTE), hWnd, AddNote);
				hwndDialog = NULL;  // Window handle of dialog box
				if (!IsWindow(hwndDialog))
				{
					hwndDialog = CreateDialog(hInst,
						MAKEINTRESOURCE(IDD_ADDNOTE),
						hWnd,
						(DLGPROC)AddNote);
					
					ShowWindow(hwndDialog, SW_SHOW);
				}
			}
				break;
			case ID_FILE_NEWNOTECTRL:
				hwndDialog = NULL;  // Window handle of dialog box
				if (!IsWindow(hwndDialog))
				{
					hwndDialog = CreateDialog(hInst,
						MAKEINTRESOURCE(IDD_ADDNOTE),
						hWnd,
						(DLGPROC)AddNote);

					ShowWindow(hwndDialog, SW_SHOW);
				}
				break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;

            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
	case WM_NOTIFY:
		switch (((LPNMHDR)lParam)->code)
		{
		case NM_DBLCLK: // Bắt sự kiện double click vào 1 note và cho hiện listview mới
		{
			WCHAR buf[512];
			// Delete tất cả item trong listview tránh thêm mới
			lineOfListView = 0;
			ListView_DeleteAllItems(hListView);

			
			HTREEITEM hT = TreeView_GetSelection(hwndTree);
			if (hT != NULL)
			{
				wsprintf(buf, L"");

				TVITEMW tvI;
				tvI.mask = TVIF_TEXT;
				tvI.hItem = hT;
				tvI.pszText = buf;
				tvI.cchTextMax = 50;


				TreeView_GetItem(hwndTree, &tvI);

				for (int i = 0; i < allNote.size(); i++)
				{
					for (int j = 0; j < allNote[i].getSizeTagOfNote(); j++)
					{
						if (wcscmp(allNote[i].getPosOfTag(j), buf) == 0)
						{
							// Thêm item vào listview
							LVITEM lvR;
							lvR.mask = LVIF_TEXT | LVCF_FMT;
							lvR.iItem = lineOfListView;
							lvR.pszText = allNote[i].getContentNote();
							lvR.piColFmt = LVCFMT_LEFT;
							lvR.iSubItem = 0;
							ListView_InsertItem(hListView, &lvR);
							ListView_SetItemText(hListView, lineOfListView, 1, allNote[i].getAllTagWCHAR());
							lineOfListView++;
							/*
							for (int i = 0; i < allNote.size(); i++)
							{
								MessageBox(hWnd, allNote[i].getContentNote(), 0, 0);
								for (int j = 0; j < allNote[i].getSizeTagOfNote(); j++)
								{
									MessageBox(hWnd, allNote[i].getPosOfTag(j), 0, 0);
								}
							}*/
						}
					}
				}
			}
		}
		break;
		case NM_RCLICK: // Bắt sự kiện click chuột vào 1 item trong list view
		{
			
			int iPos = ListView_GetNextItem(hListView, -1, LVNI_SELECTED);
			if (iPos != -1)
			{
				ListView_GetItemText(hListView, iPos, 0, content, 512);
				ListView_GetItemText(hListView, iPos, 1, tag, 512);
				DialogBox(hInst, MAKEINTRESOURCE(IDD_PREVIEW), hWnd, Preview);
			}
		}
		break;
		}
		break;
	case  WM_TRAY_ICON:
	{
		switch (lParam)
		{
		case WM_LBUTTONDBLCLK: // click chuột trái vào notification area icon
		{
			ShowWindow(hWnd, SW_SHOWNORMAL);
			break;
		}

		case WM_RBUTTONDOWN: // Chuột phải notification hiện menu
		{
			// Notification area 
			hMenu = CreatePopupMenu();
			AppendMenu(hMenu, MF_STRING, ID_NOTIFYMENU_BROWSER, L"Notes Browser");
			AppendMenu(hMenu, MF_SEPARATOR, NULL, NULL);
			AppendMenu(hMenu, MF_STRING, ID_NOTIFYMENU_CHART, L"View Statistics");
			AppendMenu(hMenu, MF_STRING, ID_NEWNOTE, L"Add Note");
			AppendMenu(hMenu, MF_SEPARATOR, NULL, NULL);
			AppendMenu(hMenu, MF_STRING, IDM_EXIT, L"Exit");

			SetMenuDefaultItem(hMenu, NULL, TRUE);

			SetForegroundWindow(hWnd);
			GetCursorPos(&pointNotify);
			TrackPopupMenu(hMenu, TPM_RIGHTBUTTON, pointNotify.x, pointNotify.y, NULL, hWnd, NULL);

			DestroyMenu(hMenu);
			hMenu = NULL;

			break;

		}
		}
		break;
	}
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		doRemoveHook(hWnd);
		tempAllTags.clear();
		tempAllTags.~vector();
		saveNoteToFile(allNote);

		if (!textAllTags)
			delete[] textAllTags;
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

bool checkExistInAllTag(WCHAR* tag)
{
	for (int i = 0; i < allTags.size(); i++)
	{
		if (wcscmp(tag, allTags[i]) == 0)
		{
			return true;
		}
	}
	return false;
}

// Message handler for new note
INT_PTR CALLBACK AddNote(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	HWND hContent = GetDlgItem(hDlg, IDC_EDIT1);
	HWND hComboBox = GetDlgItem(hDlg, IDC_COMBO1);
	hListTag = GetDlgItem(hDlg, IDC_EDIT2);

	// Tạo một note mới để lưu các thông số
	
	
	

	switch (message)
	{
		case WM_INITDIALOG:
		{
			allTags.clear();
			loadTagsFromFile();

			ZeroMemory(&tempAllTags, sizeof(tempAllTags));
			for (int i = 0; i < allTags.size(); i++)
			{
				SendMessage(hComboBox, CB_ADDSTRING, 0, (LPARAM)allTags[i]);
			}
			// Hiện cái đầu tiên
			SendMessage(hComboBox, CB_SETCURSEL, 0, 0);
			return (INT_PTR)TRUE;
		}
		break;
		case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);

			// Parse the menu selections:
			switch (wmId)
			{
				case IDC_BUTTON1: // New Tags
				{
					hNewTag = NULL;  // Window handle of dialog box
					if (!IsWindow(hNewTag))
					{
						hNewTag = CreateDialog(hInst,
							MAKEINTRESOURCE(IDD_ADDTAGS),
							hDlg,
							(DLGPROC)NewTags);
						ShowWindow(hNewTag, SW_SHOW);
					}
					break;
				}
				break;
				case IDC_BUTTON2: // Save
				{
					CNote note;
					int lenTextContent;
					// Lấy độ dài content
					lenTextContent = GetWindowTextLength(hContent);
					// Chứa nội dung content

					WCHAR* contentBuffer = new WCHAR[lenTextContent + 1];
					if (0 == lenTextContent)
					{
						MessageBox(hDlg, L"Please insert content!!!", 0, MB_OK);
						break;
					}
					if (0 == countTags)
					{
						MessageBox(hDlg, L"Please enter at least one tag!!!", 0, MB_OK);
						break;
					}

					// Tiến hành lấy nội dung Content rồi thêm vào Note.content
					GetWindowText(hContent, contentBuffer, lenTextContent + 1);
					note.addContentNote(contentBuffer);
					//addTagToNote(tempAllTags, note); // Add temp Tag to Note
					for (int i = 0; i < tempAllTags.size(); i++)
					{
						note.addTag(tempAllTags[i]);
						if (checkExistInAllTag(tempAllTags[i]) == false)
						{
							TV_INSERTSTRUCT tvS;
							TVITEM tvI;
							// Add thêm tag mới vào TreeView
							tvI.mask = TVIF_TEXT;
							tvI.pszText = (LPWSTR)tempAllTags[i];
							tvS.item.mask = TVIF_TEXT;
							tvS.hInsertAfter = TVI_LAST;
							tvS.hParent = hSearchTree;
							tvS.item = tvI;

							hChildTag = TreeView_InsertItem(hwndTree, &tvS);
							allTags.push_back(tempAllTags[i]);
						}
						
					}
					//Add to allNote
					allNote.push_back(note);

					// Add thêm note mới vào tree view
					insertNoteToTree(note);

					// reset cho lần thêm note mới
					countTags = 0;
					firstTag = true;

					// Save to File
					saveTagsToFile(allTags);
					saveNoteToFile(allNote);

					EndDialog(hDlg, LOWORD(wParam));
					if (!contentBuffer)
						delete[] contentBuffer;
					return (INT_PTR)TRUE;

				}
				break;
				case IDC_BUTTON3: // Clear
				{
					// Xóa nội dung trên dialog
					SetWindowText(hContent, L"");
					SetWindowText(hListTag, L"");
					firstTag = true;
					countTags = 0;
					tempAllTags.clear();
				}
				break;
				case IDC_BUTTON4: //Add Tags
				{
					int lenAddTagBuffer;
					
					// Lấy độ dài chuỗi combobox
					lenAddTagBuffer = GetWindowTextLength(hComboBox);
					
					// Chứa nội dung combobox
					WCHAR* addTagBuffer = new WCHAR[lenAddTagBuffer + 1];
					
					// Tiền hành lấy nội dung ComboBox rồi thêm vào vector Tag
					GetWindowText(hComboBox, addTagBuffer, lenAddTagBuffer + 1);
					
					if (true == isExistTags(tempAllTags, addTagBuffer))
					{
						MessageBox(hDlg, L"Tag already exists, please enter a different tag!!!", 0, MB_OK);
						break;
					}
					else
					{
						tempAllTags.push_back(addTagBuffer); // Thêm vào vector để lưu tạm thời
					}
					if (true == firstTag)
					{
						wcscpy(textAllTags, addTagBuffer);
						SetWindowText(hListTag, textAllTags);
						firstTag = false;
					}
					else
					{
						wsprintf(textAllTags, L"%s, %s", textAllTags, addTagBuffer);
						SetWindowText(hListTag, textAllTags);
					}
					countTags++;
					if (!addTagBuffer)
						delete[] addTagBuffer;
				}
				break;
				case IDCANCEL:
				{
					EndDialog(hDlg, LOWORD(wParam));

					// reset cho lần thêm note mới
					countTags = 0;
					firstTag = true;
					tempAllTags.clear();
					if (!textAllTags)
						delete[] textAllTags;
					return (INT_PTR)TRUE;
				}
				break;
			}
		}
			break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK NewTags(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	HWND hTextNewTag = GetDlgItem(hDlg, IDC_EDIT2);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);


		// Parse the menu selections:
		switch (wmId)
		{
		case IDCANCEL:
		{
			
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
		case IDOK:
		{
			WCHAR* newTagBuffer;
			ZeroMemory(&newTagBuffer, sizeof(newTagBuffer));
			int lenNewTagBuffer;

			// Lấy độ dài chuỗi combobox
			lenNewTagBuffer = GetWindowTextLength(hTextNewTag);
			// Tiến hành kiểm tra độ dài nếu = 0 là người dùng chưa nhập gì
			if (0 == lenNewTagBuffer)
			{
				MessageBox(hDlg, L"Please insert Tags!!!", L"ERROR", MB_OK);
				break;
			}
			else
				newTagBuffer = new WCHAR[lenNewTagBuffer + 1];			// Chứa nội dung
			// Tiền hành lấy nội dung rồi thêm vào vector Tag
			GetWindowText(hTextNewTag, newTagBuffer, lenNewTagBuffer + 1);
			// Kiểm tra tag mới tạo có tồn tại hay chưa
			if (true == isExistTags(tempAllTags, newTagBuffer))
			{
				MessageBox(hDlg, L"Tag already exists, please enter a different tag!!!", 0, MB_OK);
				break;
			}
			// Kiểm tra tag mới này đã tồn tại chưa rồi thêm vào allTAg
			if (false == isExistAllTags(allTags, newTagBuffer))
			{
				tempAllTags.push_back(newTagBuffer); // Thêm vào vector local để lưu tạm thời
				
			}
			else
			{
				MessageBox(hDlg, L"Tag already exists, please add it to Add Note Window!!!", 0, MB_OK);
				break;
			}
			
			EndDialog(hDlg, LOWORD(wParam));
			if (true == firstTag)
			{
				wcscpy(textAllTags, newTagBuffer);
				SetWindowText(hListTag, textAllTags);
				firstTag = false;
			}
			else
			{
				wsprintf(textAllTags, L"%s, %s", textAllTags, newTagBuffer);
				SetWindowText(hListTag, textAllTags);
			}
			
			countTags++;
			if (!newTagBuffer)
				delete[] newTagBuffer;
			return (INT_PTR)TRUE;
		}
		break;
		case IDC_BUTTON1:
		{
			SetWindowText(hTextNewTag, L"");
		}
		break;
		}
	}
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK Preview(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	HWND textNote = GetDlgItem(hDlg, IDC_EDIT1);
	HWND textTag = GetDlgItem(hDlg, IDC_EDIT2);
	switch (message)
	{
	case WM_INITDIALOG:
		SetWindowText(textNote, content);
		SetWindowText(textTag, tag);
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		int wmId = LOWORD(wParam);


		// Parse the menu selections:
		switch (wmId)
		{
			case IDOK:
			{

				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


void flagFiveTag()
{
	int maxOfNote = 0;
	if (saveTagPaint.size() > 5)
	{
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < saveTagPaint.size(); j++)
			{
				if ((saveTagPaint[j].getNumOfNote() > maxOfNote) && (saveTagPaint[j].getFlag() == false))
				{
					maxOfNote = saveTagPaint[j].getNumOfNote();
				}
			}
			for (int j = 0; j < saveTagPaint.size(); j++)
			{
				if (saveTagPaint[j].getNumOfNote() == maxOfNote)
				{
					saveTagPaint[j].setFlagToTrue();
				}
			}
			maxOfNote = 0;
		}
	}
	else
	{
		for (int i = 0; i < saveTagPaint.size(); i++)
		{
			for (int j = 0; j < saveTagPaint.size(); j++)
			{
				if ((saveTagPaint[j].getNumOfNote() > maxOfNote) && (saveTagPaint[j].getFlag() == false))
				{
					maxOfNote = saveTagPaint[j].getNumOfNote();
				}
			}
			for (int j = 0; j < saveTagPaint.size(); j++)
			{
				if (saveTagPaint[j].getNumOfNote() == maxOfNote)
				{
					saveTagPaint[j].setFlagToTrue();
				}
			}
			maxOfNote = 0;
		}
	}
}

// Message handler for about box.
INT_PTR CALLBACK Chart(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	vector<HWND> hTag;
	HWND hwnd;
	switch (message)
	{
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		hFont = CreateFont(lf.lfHeight, lf.lfWidth,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
		
	case WM_INITDIALOG:
		hTag.clear();

		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hDlg, &ps);
		int max = 0;

		GetClientRect(hDlg, &rc);

		FillRect(hdc, &rc, (HBRUSH)GetStockObject(WHITE_BRUSH));
		

		// Tao khung
		MoveToEx(hdc, 30, 10, NULL);
		LineTo(hdc, 30, 240);
		TextOut(hdc, 5, 0, L"Note ", 4);


		MoveToEx(hdc, 30, 240, NULL);
		LineTo(hdc, rc.right - 20, 240);
		TextOut(hdc, rc.right - 40, 250, L"Tags", 4);

		int size = saveTagPaint.size();
		int textStaticTag = 25;
		if (size > 0)
		{
			// Tao cot moc
			for (int i = 0; i < size; i++)
			{
				if (max < saveTagPaint[i].getNumOfNote())
					max = saveTagPaint[i].getNumOfNote();
				
			}

			int x = 240;
			for (int i = 0; i < max / 2 + 1; i++)
			{
				MoveToEx(hdc, 10, x - 60, NULL);

				WCHAR t[20];
				wsprintf(t, L"%d", (i + 1) * 2);
				TextOut(hdc, 5, x - 60 - 10, t, 1);

				MoveToEx(hdc, 25, x - 60, NULL);
				LineTo(hdc, 30, x - 60);
				x -= 60;
			}

			HRGN *hrg;
			hrg = new HRGN[size];


			int startx = 40;
			int starty = 240;
			int space;


			// Độ rộng
			srand(time(NULL));

			// Hiện tag kế thanh chú thích
			int posxTag = 470;
			int posyTag = 25;

			// vị trí hiện thanh chú thích
			int posxChuThich = 30;
			int posYChuThich = 40;

			// Lọc 5 tag có số note nhiều nhất và gán cờ cho chúng
			flagFiveTag();
			
			int count = 0;
			for (int i = 0; i < size; i++)
			{
				if (saveTagPaint[i].getFlag() == false || count >= 5)
				{
					continue;
				}

				int x = (rand() % 250) + 200;
				int y = (rand() % 250) + 200;
				int z = (rand() % 250) + 200;


				// Chiều cao
				int temp = saveTagPaint[i].getNumOfNote() * 30;
				space = 50;
				hrg[i] = CreateRectRgn(startx, starty - temp, startx + space, starty);
				
				FillRgn(hdc, hrg[i], CreateSolidBrush(RGB(x, y, z)));

				// Khoảng cách các hình cột >> Tăng lên
				startx = startx + space + 10;

				// In các tag kèm theo hình chú thích
				hwnd = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | ES_READONLY, 470, textStaticTag, 70, 15, hDlg, NULL, hInst, NULL);
				SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);
				hTag.push_back(hwnd);
				textStaticTag += 30;
				SetWindowTextW(hTag[i], saveTagPaint[i].getContentTag());

				// Tặng vị trí tag 
				posyTag += 30;

				count++;
				// In hình chú thích
				HRGN hrg = CreateRectRgn(400, posxChuThich, 450, posYChuThich);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(x, y, z)));
				//Tăng vị trí màu chú thích
				posxChuThich += 30;
				posYChuThich += 30;
			}

		}

		EndPaint(hDlg, &ps);

		DeleteDC(hdc);

		InvalidateRect(hDlg, &rc, FALSE);
	}
		break;
	}
	return (INT_PTR)FALSE;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void doInstallHook(HWND hWnd)
{
	// gọi hàm DLL theo kiểu Run-time
	// Định nghĩa prototype của hàm
	typedef VOID(*MYPROC)(HWND);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	// load DLL và lấy handle của DLL module
	hinstLib = LoadLibrary(L"HOOKNoteBrowserDLL.dll");
	// Nếu load thành công, lấy địa chỉ của hàm DrawCircle trong DLL
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doInstallHook");
		// Nếu lấy được địa chỉ hàm, gọi thực hiện hàm
		if (ProcAddr != NULL)
			ProcAddr(hWnd);
	}
}

void doRemoveHook(HWND hWnd)
{
	// gọi hàm DLL theo kiểu Run-time
	// Định nghĩa prototype của hàm
	typedef VOID(*MYPROC)(HWND);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	// load DLL và lấy handle của DLL module
	hinstLib = LoadLibrary(L"HOOKNoteBrowserDLL.dll");
	// Nếu load thành công, lấy địa chỉ của hàm DrawCircle trong DLL
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doRemoveHook");
		// Nếu lấy được địa chỉ hàm, gọi thực hiện hàm
		if (ProcAddr != NULL)
			ProcAddr(hWnd);
	}
}

void insertNoteToTree(CNote note)
{
	WCHAR buf[50];
	TV_INSERTSTRUCT tvS;	// Thực hiện lưu vào trong TreeView để tiến hành Preview
	TVITEM tvI;
	HTREEITEM hItem;
	for (int i = 0; i < note.getSizeTagOfNote(); i++)
	{
		// Lấy htreeitem trùng vs tag của note
		HTREEITEM result = searchItemWithTagName(note.getPosOfTag(i));
		StringCchCopy(buf, 50, note.getContentNote());
		if (result == hSearchTree)	// Nếu là root thì tạo mới luôn giá trị cho Tag
		{
			tvI.mask = TVIF_TEXT;
			tvI.pszText = (LPWSTR)note.getPosOfTag(i);

			tvS.item.mask = TVIF_TEXT;
			tvS.hInsertAfter = TVI_LAST;
			tvS.hParent = result;
			tvS.item = tvI;

			hItem = TreeView_InsertItem(hwndTree, &tvS);

			tvI.mask = TVIF_TEXT;
			tvI.pszText = buf;
			tvI.cchTextMax = 50;

			tvS.hParent = hItem;
			tvS.item.mask = TVIF_TEXT;
			tvS.item = tvI;

			TreeView_InsertItem(hwndTree, &tvS);
		}
		else	// Nếu không phải thì chỉ add vào giá trị tag đã có.
		{

			tvI.mask = TVIF_TEXT;
			tvI.pszText = buf;
			tvI.cchTextMax = 50;

			tvS.hParent = result;
			tvS.item.mask = TVIF_TEXT;
			tvS.item = tvI;

			TreeView_InsertItem(hwndTree, &tvS);
		}
		TreeView_SortChildren(hwndTree, result, TRUE);
	}
	
}



BOOL OnCreate(HWND hWnd)
{
	
	INITCOMMONCONTROLSEX icc;
	icc.dwSize = sizeof(icc);
	icc.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&icc);
	
	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	hFont = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);
	//HWND hTreeView = CreateATreeView(hWnd);
	RECT rc;
	GetClientRect(hWnd, &rc);

	// Tạo tree view
	hwndTree = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		WC_TREEVIEW,
		0,
		WS_CHILD | WS_VISIBLE | TVS_SHOWSELALWAYS | TVS_HASBUTTONS |
		TVS_LINESATROOT | TVS_HASLINES ,
		10, 10, 200, 250,
		hWnd, NULL, hInst, NULL);
	// Listview//////////////////////////////////////////////////////////////////////////////////////////////////
	hListView = CreateWindow(WC_LISTVIEW, L"", WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_REPORT | LVS_EDITLABELS, 230, 10, 350, 250, hWnd, NULL, hInst, NULL);
	SendMessage(hListView, WM_SETFONT, (WPARAM)hFont, TRUE);
	// Tạo các cột của list view để hiển thị danh sách các chi tiêu
	LVCOLUMN lvCol1, lvCol2;

	// Cột 1 Loại thu phí
	lvCol1.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol1.fmt = LVCFMT_LEFT;
	lvCol1.pszText = L"Content";
	lvCol1.cx = 150;
	ListView_InsertColumn(hListView, 0, &lvCol1);

	// Cột 2 Nội dung
	lvCol2.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol2.fmt = LVCFMT_LEFT;
	lvCol2.pszText = L"Tags";
	lvCol2.cx = 200;
	ListView_InsertColumn(hListView, 1, &lvCol2);

	// Listview//////////////////////////////////////////////////////////////////////////////////////////////////
	TV_INSERTSTRUCT tvS;	// Thực hiện lưu vào trong TreeView để tiến hành Preview
	TVITEM tvI;
	HTREEITEM htreeChild;
	tvI.mask = TVIF_TEXT;
	tvI.pszText = L"All Tags";
	tvI.iImage = 0;
	tvI.iSelectedImage = 1;

	tvS.hParent = NULL;
	tvS.hInsertAfter = TVI_ROOT;
	tvS.item.mask = TVIF_TEXT;
	tvS.item = tvI;

	hSearchTree = TreeView_InsertItem(hwndTree, &tvS);


	for (int i = 0; i < allNote.size(); i++)
	{
		insertNoteToTree(allNote[i]);
	}

	return (INT_PTR)TRUE;
	return true;
}




bool isExistTags(vector<WCHAR*> localAllTags, WCHAR* tag)
{
	if (localAllTags.size() == 0)
		return false;
	for (int i = 0; i < localAllTags.size(); i++)
	{
		if (wcscmp(localAllTags[i], tag) == 0)
		{
			return true;
		}
	}
	return false;
}

void addTagToNote(vector<WCHAR*> tempAllTag, CNote note)
{
	for (int i = 0; i < tempAllTag.size(); i++)
	{
		note.addTag(tempAllTag[i]);
	}
}

bool isExistAllTags(vector<WCHAR*> allTags, WCHAR* tag)
{
	if (allTags.size() == 0)
		return false;
	for (int i = 0; i < allTags.size(); i++)
	{
		if (wcscmp(allTags[i], tag) == 0)
		{
			return true;
		}
	}
	return false;
}

void saveNoteToFile(vector<CNote> allNotes)
{
	wfstream wfile;
	wfile.open("saveNotes.txt", ios::out);

	if (wfile.is_open())
	{
		// Dùng để ghi unicode
		locale unicode(locale(), new codecvt_utf8<WCHAR>);
		wfile.imbue(unicode);

		int temp = 0;
		for (int i = 0; i < allNotes.size(); i++)
		{
			wfile << allNotes[i].getContentNote() << endl;
			int sizeTags = allNotes[i].getSizeTagOfNote();
			if (sizeTags > 1)
			{
				for (int j = 0; j < sizeTags - 1; j++)
				{
					wfile << allNotes[i].getPosOfTag(j) << "-";
					temp = j;
				}
				wfile << allNotes[i].getPosOfTag(temp + 1) << endl;
			}
			else
			{
				wfile << allNotes[i].getPosOfTag(temp) << endl;	
			}
		}
		wfile.close();
	}
	else
	{
		MessageBox(0, L"Cannot create file!!!", 0, MB_OK | MB_ICONERROR);
		return;
	}
	
}

CNote splitTags(WCHAR* content, WCHAR* alltag)
{
	CNote note;
	
	// Thêm content vào note
	note.addContentNote(content);

	//Tách chuỗi tag ra thành từng tag nhỏ đc phân cách bởi -
	// Xu ly Tag;
	wstring proc = alltag;
	int size = proc.size();

	WCHAR *tag;
	tag = new WCHAR[500];	// Dùng con trỏ để sử dụng, tránh bị tác động vùng nhớ đã lưu trước đó

	int pos = 0;
	int count = 0;
	int sumTags = 0;
	while (pos <= size)	// Thực hiện tách chuỗi Tag theo yêu cầu.
	{
		if (alltag[pos] != '-' && pos != size)
		{
			tag[count] = alltag[pos];
			count++;
			pos++;
		}
		else
		{
			sumTags++;
			tag[count] = '\0';
			wstring check = tag;
			if (check.size() != 0) // Nếu tag rỗng ko lưu
			{
				note.addTag(tag);	// Lưu lại các tag được phân cách.
			}

			tag = new WCHAR[500]; // Thực hiện tạo mới con trỏ để lưu giá trị tiếp theo.
			if (alltag[pos + 1] == ' ')
			{
				pos++;
			}
			count = 0;
			pos++;
		}
	}

	delete tag;
	return note;
}

void loadNotesFromFile()
{
	wfstream rfile;
	rfile.open("saveNotes.txt", ios::in);

	if (rfile.is_open())
	{

		// load file unicode
		locale unicode(locale(), new codecvt_utf8<WCHAR>);
		rfile.imbue(unicode);
		WCHAR *buffer1;
		WCHAR *buffer2;
		CNote note;
		while (!rfile.eof())
		{
			int countTag = 0;
			buffer1 = new WCHAR[1024];
			buffer2 = new WCHAR[1024];
			rfile.getline(buffer1, 1024);
			if (wcscmp(buffer1, L"") != 0)
			{
				rfile.getline(buffer2, 1024);
				note = splitTags(buffer1, buffer2);

				allNote.push_back(note);
			}
			
			if (!buffer1)
				delete[] buffer1;
			if (!buffer2)
				delete[] buffer2;
		}

		rfile.close();
	}
	else
	{
		MessageBox(0, L"Cannot open saveTags.dat!!!", 0, MB_OK | MB_ICONERROR);
		return;
	}

}

void saveTagsToFile(vector<WCHAR*> allTag)
{
	wfstream wfile;
	wfile.open("saveTags.txt", ios::out);

	if (wfile.is_open())
	{
		// Dùng để ghi unicode
		locale unicode(locale(), new codecvt_utf8<WCHAR>);
		wfile.imbue(unicode);

		for (int i = 0; i < allTag.size(); i++)
		{
			wfile << allTag[i] << endl;
		}
		wfile.close();
	}
	else
	{
		MessageBox(0, L"Cannot create file!!!", 0, MB_OK | MB_ICONERROR);
		return;
	}
	
}

void loadTagsFromFile()
{
	wfstream rfile;
	rfile.open("saveTags.txt", ios::in);
	if (rfile.is_open())
	{
		// load file unicode
		locale unicode(locale(), new codecvt_utf8<WCHAR>);
		rfile.imbue(unicode);
		WCHAR *buffer;
		while (!rfile.eof())
		{
			buffer = new WCHAR[1024];
			rfile.getline(buffer, 1024);
			if (wcscmp(buffer, L"") != 0)
			{
				allTags.push_back(buffer);
			
			}
			if (!buffer)
				delete[] buffer;
		}

		rfile.close();	
	}
	else
	{
		MessageBox(0, L"Cannot open saveTags.dat!!!", 0, MB_OK | MB_ICONERROR);
		return;
	}

}


// Duyệt qua Tree View để lấy HTREEITEM

WCHAR * GetTitle(HTREEITEM hT)
{
	WCHAR buf[500];

	TVITEMEX tvI; // Tạo ITEM để lấy giá trị của tag.
	tvI.hItem = hT;
	tvI.mask = TVIF_TEXT;
	tvI.pszText = buf;
	tvI.cchTextMax = 500;

	BOOL flag = TreeView_GetItem(hwndTree, &tvI);
	if (flag)	// Nếu lấy thành công thì so trả về chuỗi, ko thì trả về kí tự trắng.
	{
		return tvI.pszText;
	}
	else
	{
		return L"";
	}
}

HTREEITEM searchItemWithTagName(WCHAR* temp)
{
	HTREEITEM hCur = TreeView_GetChild(hwndTree, hSearchTree);

	while (hCur != NULL) // Kiem tra khac NULL khi la root hoac ko co phan tu trung.
	{
		if (_wcsicmp(GetTitle(hCur), temp) == 0) // So sánh chuỗi kí tự để lấy địa chỉ HTREEITEM
		{
			return hCur;
		}

		hCur = TreeView_GetNextSibling(hwndTree, hCur);  // Duyệt qua các phần tử tag mới để so sánh
	}

	return hSearchTree;
}



void getDataToChart()
{
	for (int i = 0; i < allNote.size(); i++)
	{
		for (int j = 0; j < saveTagPaint.size() ; j++)
		{
			for (int k = 0; k < allNote[i].getSizeTagOfNote(); k++)
			{
				if (wcscmp(saveTagPaint[j].getContentTag(), allNote[i].getPosOfTag(k)) == 0)
				{
					saveTagPaint[j].addNumOfNote();
					break;
				}
			}
		}
	}
}