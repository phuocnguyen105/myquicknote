#pragma once

#include "resource.h"
#include <shellapi.h>
#include <strsafe.h>
#include <CommCtrl.h>
#include <fstream>
#include <cwchar>
#include "CNote.h"
#include <codecvt>	// Dùng cho unicode
#include <locale>
#include "CTag.h"
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

using namespace std;
//Biến
NOTIFYICONDATA notifyApp = { 0 }; // Biến để add notification area
HMENU hMenu; // menu notify
POINT pointNotify; 

// Vector lưu tất cả node và tag
vector<CNote> allNote;
vector<WCHAR*> allTags;
vector<CTag> saveTagPaint;
//Tree
HWND hwndTree;
HTREEITEM hSearchTree;
HTREEITEM hChildTag;
vector<HTREEITEM> htreeitem;
//Hàm -------------------------------------------------------
BOOL OnCreate(HWND hWnd);
//Tạo tree view
void insertNoteToTree(CNote note);
HTREEITEM searchItemWithTagName(WCHAR* temp);
void insertNoteToTree(CNote note);

// Kiểm tra tag đã tồn tại hay chưa
bool isExistTags(vector<WCHAR*> localAllTags, WCHAR* tag); // Kiểm tra tag đã tồn tại khi thêm note ko
bool isExistAllTags(vector<WCHAR*> allTags, WCHAR* tag); // Kiểm tra trong tất cả các tag "tag" đã tồn tại hay chưa

void addTagToNote(vector<WCHAR*> tempAllTag, CNote note); // Add all tag to Note

// FILE
void saveTagsToFile(vector<WCHAR*> allTag);
void loadTagsFromFile();
void saveNoteToFile(vector<CNote> allNotes);
void loadNotesFromFile();

//Vẽ
void getDataToChart();
RECT rc;

WCHAR* textAllTags = new WCHAR[1000];
bool firstTag = true;
int countTags = 0;
vector<WCHAR*> tempAllTags;
HWND hListTag; // Toàn cục vì cần dùng cho new tags

//Hook
void doInstallHook(HWND);
void doRemoveHook(HWND);

int lineOfListView;
HWND hListView;

WCHAR content[512];
WCHAR tag[512];
//------------------------------------------------------------
// ID
#define WM_TRAY_ICON WM_USER
#define ID_NOTIFYMENU_BROWSER	995
#define ID_NOTIFYMENU_CHART		996
#define ID_NOTIFYMENU_EXIT		997
#define ID_NEWNOTE				998
