//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyQuickNote.rc
//
#define IDC_MYICON                      2
#define IDD_MYQUICKNOTE_DIALOG          102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MYQUICKNOTE                 107
#define IDI_SMALL                       108
#define IDC_MYQUICKNOTE                 109
#define IDR_MAINFRAME                   128
#define IDD_ADDNOTE                     130
#define IDR_MENU1                       136
#define IDI_ICON1                       138
#define IDI_NOTIFYAPP                   138
#define IDD_ADDTAGS                     139
#define IDD_PREVIEW                     140
#define IDD_CHART                       141
#define IDI_ICON2                       142
#define IDC_MFCMENUBUTTON1              1035
#define IDC_MFCFONTCOMBO1               1036
#define IDC_COMBO1                      1037
#define IDC_COMBO2                      1038
#define IDC_COMBOBOXEX1                 1039
#define IDC_EDIT1                       1047
#define IDC_EDIT2                       1048
#define IDC_BUTTON1                     1049
#define IDC_BUTTON2                     1050
#define IDC_BUTTON3                     1051
#define IDC_BUTTON4                     1052
#define ID_FILE_NEWNOTECTRL             32771
#define ID_TAG_ADDTITLE                 32774
#define ID_TAG_COLOR                    32775
#define ID_TAG_TAG                      32776
#define ID_TAG_555                      32777
#define ID_TAG_NEWTAG                   32778
#define ID_TAG_J                        32779
#define ID_NEWNOTE                      32780
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1053
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
