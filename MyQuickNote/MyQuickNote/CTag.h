#pragma once
class CTag
{
	WCHAR content[512];
	int numOfNote;
	bool flag; // flag = true đã vẽ
				// flag = false chưa vẽ
public:
	CTag();
	~CTag();
	WCHAR* getContentTag();
	int getNumOfNote();
	void setContentTag(WCHAR* content);
	void addNumOfNote();
	void setFlagToTrue();
	bool getFlag();
};

