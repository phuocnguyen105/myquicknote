#pragma once
#include <iostream>
#include <vector>

using namespace std;

class CNote
{
private:
	WCHAR content[256];
	vector<WCHAR*> tagOfNote;
public:
	CNote();
	~CNote();

	void addTag(WCHAR* tag); // Thêm 1 tag
	void addContentNote(WCHAR* content); // Thêm nội dung note
	vector<WCHAR*> getVectorTag(); // Get Tag dạng vector
	WCHAR* getAllTagWCHAR(); // Get tag dạng WCHAR*
	WCHAR* getContentNote(); // Get nội dung note
	int getSizeTagOfNote(); // Get tổng số tag của note
	WCHAR* getPosOfTag(int i); // Get tag ở vị trí bất kỳ
};

