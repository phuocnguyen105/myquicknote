#include "stdafx.h"
#include "CTag.h"


CTag::CTag()
{
	numOfNote = 0;
	flag = false;
}


CTag::~CTag()
{
}

WCHAR* CTag::getContentTag()
{
	return content;
}

int CTag::getNumOfNote()
{
	return numOfNote;
}

void CTag::setContentTag(WCHAR* content)
{
	wsprintf(this->content, L"%s", content);
}

void CTag::addNumOfNote()
{
	numOfNote++;
}

void CTag::setFlagToTrue()
{
	this->flag = true;
}

bool CTag::getFlag()
{
	return flag;
}