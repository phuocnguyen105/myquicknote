#include "stdafx.h"
#include "CNote.h"


CNote::CNote()
{
	tagOfNote.clear();
}


CNote::~CNote()
{
	tagOfNote.clear();
	tagOfNote.~vector();
}

void CNote::addTag(WCHAR* tag)
{
	tagOfNote.push_back(tag);
}

void CNote::addContentNote(WCHAR* content)
{
	wsprintf(this->content, L"%s", content);
}

vector<WCHAR*> CNote::getVectorTag()
{
	return tagOfNote;
}

WCHAR* CNote::getAllTagWCHAR()
{
	WCHAR* buf = new WCHAR[1024];

	wsprintf(buf, L"%s", tagOfNote[0]);

	for (int i = 1; i < tagOfNote.size(); i++)
	{
		wsprintf(buf, L"%s, %s", buf, tagOfNote[i]);
	}

	return buf;
}

WCHAR* CNote::getContentNote()
{
	return content;
}

int CNote::getSizeTagOfNote()
{
	return tagOfNote.size();
}

WCHAR* CNote::getPosOfTag(int i)
{
	return tagOfNote[i];
}