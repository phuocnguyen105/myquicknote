// HOOKNoteBrowserDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <Windowsx.h>

#define EXPORT  __declspec(dllexport)

HHOOK hHook = NULL;
HINSTANCE hinstLib;
bool invisibleNode = false;
HWND targethWnd;
LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0) // không xử lý message 
		return CallNextHookEx(hHook, nCode, wParam, lParam);
	// xử lý message: Ctrl + Right mouse click
	if (((DWORD)lParam & 0x40000000) && (HC_ACTION == nCode))
	{
		if ((wParam == VK_SPACE))
		{
			int pressWindowsKey = GetAsyncKeyState(VK_LWIN);
			int pressControlKey = GetAsyncKeyState(VK_CONTROL);
			int pressShiftKey = GetAsyncKeyState(VK_SHIFT);
			if ((0 != pressControlKey) && (0 != pressShiftKey))
			{
				if (true == invisibleNode)
				{
					// Đang ẩn nên sẽ hiện lên
					invisibleNode = false;
					ShowWindow(targethWnd, SW_NORMAL);
					//MessageBox(0, L"Hiện", 0, 0);
				}
				else if (false == invisibleNode)
				{
					// Đang hiện nên sẽ ẩn đi
					invisibleNode = true;
					ShowWindow(targethWnd, SW_MINIMIZE);
					//MessageBox(0, L"Ẩn", 0, 0);
				}
			}
		}
		return TRUE;
	}


	return CallNextHookEx(hHook, nCode, wParam, lParam);
}

EXPORT void _doInstallHook(HWND hWnd)
{

	unsigned long processID = 0;

	targethWnd = FindWindowA("MyQuickNote", NULL);
	DWORD ID = GetWindowThreadProcessId(targethWnd, &processID);

	hHook = SetWindowsHookEx(WH_KEYBOARD, (HOOKPROC)KeyboardProc, hinstLib, NULL);

	int jx = GetLastError();
	//MessageBox(hWnd, L"Install hook successfully", L"Result", MB_OK);
}

EXPORT void _doRemoveHook(HWND hWnd)
{
	if (hHook == NULL) return;
	UnhookWindowsHookEx(hHook);
	hHook = NULL;
	//MessageBox(hWnd, L"Remove hook successfully", L"Result", MB_OK);
}

